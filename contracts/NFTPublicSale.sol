// pragma solidity ^0.8.7;



// contract NFTPrivateSale  {
     
//      address public _admin;
     
//      uint public betaTotalSupply = 700000;
//      uint public alphaTotalSupply = 5100;
//      uint public genesisTotalSupply =1000;
//      uint public immortalTotalSupply =50;
     

//       //Prices
//      uint betaPrice = 30;
//      uint alphaPrice = 300;
//      uint genesisPrice = 1000;
//      uint immortalPrice = 10000;


//      mapping(address=>uint) public beta;
//      mapping(address=>uint) public alpha;
//      mapping(address=>uint) public genesis;
//      mapping(address=>uint) public immortal;
//      mapping(address=>uint) public totalAmoutPaied;
    
//      bool isNftPublicSaleStillRunning = true;
     
//      uint startDate = 1_637_150_400; //TODO change to actul date
//      uint endDate = 1_637_154_000;  //TODO change to actul date
     
    

    
//     event Deposit(address indexed _from, uint _value);

//     constructor() {
//         _admin = msg.sender;
//     }
    
//      modifier onlyAdmin() {
//         require(_admin == msg.sender);
//         _;
//     }
    
    
//     function closeNftPrivatesales() public onlyAdmin{
//         isNftPublicSaleStillRunning = false;
//     }
    
    
//     function buy(uint nft,uint amount) public payable {
//         require(block.timestamp >= startDate && block.timestamp <= endDate,
//             'Deposit rejected, NFT Public Sale has either not yet started or ended');
//         require(!isNftPublicSaleStillRunning, 'Admin has ended NFT public Sale');
//         require(amount>2000,'Not enough USDT');
        
//         //Beta
//         if(nft ==1){
//             require(amount * betaPrice == msg.value,'Not enough USDT');
//             require(betaTotalSupply>0,'Beta already sold out!');
//             emit Deposit(msg.sender, msg.value);
//             betaTotalSupply = betaTotalSupply-=amount;
//             beta[msg.sender] +=amount;
//             totalAmoutPaied[msg.sender]= totalAmoutPaied[msg.sender] +=msg.value;
          
//             //Alpha
//         }else if (nft ==2){
//             require(amount * alphaPrice == msg.value,'Not enough USDT');
//             require(alphaTotalSupply>0,'Alpha already sold out!');
//             emit Deposit(msg.sender, msg.value);
//             alphaTotalSupply = alphaTotalSupply-=amount;
//             alpha[msg.sender] +=amount;
//             totalAmoutPaied[msg.sender]= totalAmoutPaied[msg.sender] +=msg.value;
          
//             //Genesis
//         }else if (nft == 3){
//             require(amount * genesisPrice == msg.value,'Not enough USDT');
//             require(genesisTotalSupply>0,'Genesis already sold out!');
//             emit Deposit(msg.sender, msg.value);
//             genesisTotalSupply = genesisTotalSupply-=amount;
//             genesis[msg.sender]= genesis[msg.sender] +=amount;
//             totalAmoutPaied[msg.sender]= totalAmoutPaied[msg.sender] +=msg.value;
          
//             // Immortal
//         }else if(nft ==4){
//             require(amount * immortalPrice == msg.value,'Not enough USDT');
//             require(immortalTotalSupply>0,'Immortal already sold out!');
//             emit Deposit(msg.sender, msg.value);
//             immortalTotalSupply = immortalTotalSupply-=amount;
//             immortal[msg.sender]= immortal[msg.sender] +=amount;
//             totalAmoutPaied[msg.sender]= totalAmoutPaied[msg.sender] +=msg.value;
//         }
        
        
//     }
      
//       function withdrawAll() external onlyAdmin {
//         payable(_admin).transfer(address(this).balance);
//     }
// }
