// SPDX-License-Identifier: MIT
pragma solidity ^0.8.7;

interface IErc20Contract {
    // External ERC20 contract
    function transfer(address recipient, uint256 amount)
        external
        returns (bool);

    function balanceOf(address tokenOwner) external view returns (uint256);

    function approve(address spender, uint256 amount) external returns (bool);

    function transferFrom(
        address from,
        address to,
        uint256 amount
    ) external returns (bool);
}

struct VestingIndex {
    address _wallet;
    uint256 _amount;
    uint256 _date;
}

contract LOAStaking {
    IErc20Contract public _erc20Contract; // External ERC20 contract

    address _admin;
    uint256 _startDate;
    uint256 _endDate;
    mapping(address => uint256) public _totalAddressAmount;
    VestingIndex public _vestingIndex;
    VestingIndex[] public _vesting;
    uint256 public _totalAmount;
    mapping(address => VestingIndex[]) _vestedAddress;

    constructor(
        address erc20Contract,
        uint256 startDate,
        uint256 endDate
    ) {
        _admin = msg.sender;
        _erc20Contract = IErc20Contract(erc20Contract);
        startDate = _startDate;
        endDate = _endDate;
    }

    // receive erc20 token with the amount
    function receiveErc20Token(uint256 amount) external {
        require(_startDate <= block.timestamp, "Event has not started yet");
        require(_endDate < block.timestamp, "Event has ended");

        bool transfer = _erc20Contract.transferFrom(
            msg.sender,
            address(this),
            amount
        );
        if (transfer) {
            _vestingIndex = VestingIndex(
                msg.sender,
                msg.value,
                block.timestamp
            );
            _totalAddressAmount[msg.sender] += msg.value;
            _totalAmount += msg.value;
            _vesting.push(_vestingIndex);
            _vestedAddress[msg.sender].push(_vestingIndex);
        }
    }

    function returnErc20Token() external {
        uint256 length = _vestedAddress[msg.sender].length;
        if (length > 0) {
            for (uint256 i = 0; i < length; i++) {
                uint256 date = _vestedAddress[msg.sender][i]._date + 30 days;
                if (block.timestamp >= date) {
                    _erc20Contract.transfer(
                        _vestedAddress[msg.sender][i]._wallet,
                        _vestedAddress[msg.sender][i]._amount
                    );
                }
            }
        }
    }

    modifier onlyAdmin() {
        // Is Admin?
        require(_admin == msg.sender);
        _;
    }

    function returnAddresses() external view returns (VestingIndex[] memory) {
        return _vesting;
    }

    // Reject all direct deposit to this contract
    receive() external payable {
        revert();
    }
}
